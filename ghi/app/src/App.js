import Nav from './Nav'
import "./App.css"
import AttendeesList from './AttendeeList';
import LocationForm from './LocationForm';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

const router = createBrowserRouter([
  {
    path: "locations",
    children: [
      { path: "new", element: <LocationForm /> },
    ]
  }
])

return <RouterProvider router={router} />

  // return (
  //   <>
  //   <Nav />
  //   <div className="container">
  //     <LocationForm />
  //     {/* <AttendeesList attendees={props.attendees} /> */}
  //     {/* <table className="table table-striped">
  //       <thead>
  //         <tr>
  //           <th>Name</th>
  //           <th>Conference</th>
  //         </tr>
  //       </thead>
  //       <tbody>
  //         {props.attendees.map(attendee => {
  //           return (
  //             <tr key={attendee.href}>
  //               <td>{ attendee.name }</td>
  //               <td>{ attendee.conference }</td>
  //             </tr>
  //           );
  //         })}
  //       </tbody>
  //     </table> */}
  //   </div>
  //   </>
  // );
}

export default App;
