window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        const option = document.createElement('option');
        const details = state.abbreviation;
        const stateName = state.name;
        option.value = details;
        option.innerHTML = stateName;
        selectTag.appendChild(option);
      }
    }
  });