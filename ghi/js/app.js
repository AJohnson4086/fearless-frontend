window.addEventListener('DOMContentLoaded', async () => {
    function createCard(name, description, pictureUrl) {
        return `
        <div class="col">
            <div class="card grid gap-0 row-gap-3">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${name}</h5>
                    <p class="card-text">${description}</p>
                </div>
            </div>
        </div>
        `;
      }


    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
            // console.error('Response failed. Error code', error);
            console.log("response is not ok");
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(name, description, pictureUrl);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {
        console.log(e);
    }

});